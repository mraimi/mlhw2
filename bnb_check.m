function [ output ] = bnb_check( preds, testlabels )

    total = length(testlabels);
    wrong = 0;
    for i=1:length(preds)
        
        if  preds(i,1) ~= testlabels(i,1)
            wrong = wrong + 1;
        end
    end

    output = wrong/total;
end

