function [ preds ] = hw2_test_bnb( params, test )
    classes = params.classes;
    priors = params.priors;
    p_jk = params.p_jk;
    preds = zeros(size(test,1),1);
    log_priors = log(priors);
    log_p_jk = log(p_jk);
    log_p_jk_inv = log(1 - p_jk);
    test_inv = 1 - test;
    
    mult1 = test*log_p_jk';
    mult2 = test_inv*log_p_jk_inv';
    
    mult3 = mult1+mult2;
    
    for i=1:length(log_priors)
        mult3(:,i) = mult3(:,i)+log_priors(i);
    end
    
    for i=1:size(mult3,1)
        max = 0.0;
        max_index = 0;
        for j=1:size(mult3,2)
            if max == 0.0
                max = mult3(i,j);
            end
            if mult3(i,j)>=max
                max_index = j;
                max = mult3(i,j);
            end
        end
        preds(i,1) = classes(max_index);
    end
end

