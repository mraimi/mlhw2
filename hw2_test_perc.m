function [ preds ] = hw2_test_perc( params, testdata )
    w = params.w;
    theta = params.theta;
    preds = zeros(size(testdata,1),1);
    res = w'*testdata';
    for i=1:length(res)
        if res(i) <= theta
            preds(i) = -1;
        else
            preds(i) = 1;
        end
    end
end

