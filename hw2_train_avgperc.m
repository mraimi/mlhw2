function [ params ] = hw2_train_avgperc( X,Y,num_passes )
    w = zeros(size(X,2),1);
    u = zeros(size(X,2),1);
    c = 1;
    theta = 0.0;
    beta = 0.0;
    
    for i=1:num_passes
        for j=1:size(X,1)
            if Y(j,1)*(X(j,:)*w + theta)<= 0
                w = w + Y(j,1)*(X(j,:)');
                theta = theta + Y(j,1);
                u = u + Y(j,1)*c*(X(j,:)');
                beta = beta + Y(j,1)*c;
            end
            c = c + 1;
        end
    end
    params = struct('theta', theta - ((1/c)*beta), 'w', w - ((1/c)*u));


end

