function [params] = hw2_train_bnb( data, labels )
    classes = (unique(labels));
    priors = zeros(length(classes),1);
    p_jk = zeros(length(classes),size(data,2));
    for j=1:length(classes)
       logic_Y = labels == classes(j);
       logic_X = data(logic_Y,:);
       sum_X_num = bsxfun(@plus,sum(logic_X,1),1);
       sum_logic_y = sum(logic_Y)+2;
       priors(j,1) = (sum_logic_y-2)/length(labels);
       p_jk(j,:) = bsxfun(@rdivide,sum_X_num,sum_logic_y);  
    end    
    params = struct('priors', priors, 'p_jk', p_jk, 'classes', classes);
end

