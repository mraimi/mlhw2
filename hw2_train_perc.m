function [ params ] = hw2_train_perc(X,Y,num_passes)
    w = zeros(size(X,2),1);
    theta = 0.0;
    
    for i=1:num_passes
        for j=1:size(X,1)
            a = X(j,:)*w + theta;
            if Y(j,1)*a<= 0
                w = w + Y(j,1)*(X(j,:)');
                theta = theta + Y(j,1);
            end
        end
    end
    params = struct('theta', theta, 'w', w);
end

